//
// Created by Mpho Mbotho on 2020-10-06.
//

#include <string>

class UnitTestable {
public:
    UnitTestable() = default;
    UnitTestable(std::string name, int age)
        : mName{std::move(name)},
          mAge(age)
    {}
private suil_ut:
    std::string mName{"John Doe"};
    int mAge{50};
};

#ifdef SUIL_UNITTEST
#include "catch2/catch.hpp"

TEST_CASE("SuilUnitTest cmake functionality test", "[SuilUnitTest]")
{
    UnitTestable ut1;
    REQUIRE(ut1.mAge == 50);
    REQUIRE(ut1.mName == "John Doe");

    UnitTestable ut2{"Jane Doe", 25};
    REQUIRE(ut2.mAge == 25);
    REQUIRE(ut2.mName == "Jane Doe");
}
#endif